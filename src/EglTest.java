import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * mai 2016
 *
 * @author Mirko Steinle
 */
public class EglTest
{

    @Test
    public void testRecherche_premier() throws Exception
    {
        // 1. Préparation
        ArrayList<Integer> liste = new ArrayList<>(Arrays.asList(1, 2, 3));

        // 2. Exécution du code à tester
        boolean resultat = Egl.rechercheFausseBool(1, liste);

        // 3. Vérification du résultat
        Assert.assertEquals(true, resultat);
    }

    @Test
    public void testRecherche_absent() throws Exception
    {
        // 1. Préparation
        ArrayList<Integer> liste = new ArrayList<>(Arrays.asList(1, 2, 3));

        // 2. Exécution du code à tester
        boolean resultat = Egl.rechercheFausseBool(99, liste);

        // 3. Vérification du résultat
        Assert.assertEquals(false, resultat);
    }

    @Test
    public void testRecherche_milieu() throws Exception
    {
        // 1. Préparation
        ArrayList<Integer> liste = new ArrayList<>(Arrays.asList(1, 2, 3));

        // 2. Exécution du code à tester
        boolean resultat = Egl.rechercheFausseBool(2, liste);

        // 3. Vérification du résultat
        Assert.assertEquals(true, resultat);
    }

    // TODO: 1) Ajouter d'autres tests pour mettre en évidence le plus grand nombre de problèmes du code
    // => Que se passe-t-il pour une liste vide? Quand il y a des doublons? Quand on cherche le dernier élément?
    // TODO: 2) Réduire la redondance du code de test. Comment éviter le copier-coller?

}