import java.util.ArrayList;

class Egl
{
    static boolean rechercheFausseBool(int valeur, ArrayList<Integer> liste)
    {
    	for (int pos = 0; pos < liste.size() - 1; pos++)
    	{
   		 if ( valeur == liste.get(pos) )
   			 return true; // trouvé !
   		 else
   			 return false; // pas trouvé !!
    	}
     	return false;// pour que ça compile //v1
    }

   
    //v6 : vers l'automatisation des tests
	// dans la classe EglTest
}

class TestEgl
{
    public static void main (String[] args)
    {
		ArrayList<Integer> liste = remplirAvec(new Integer[]{30, 67, 15, 85, 96, 83, 25, 88, 31, 27, 35, 31});

		System.out.println (liste);// pour vérifier
		
		verifierResultatTest(30, liste, true);
			
		verifierResultatTest(33, liste, false); 
		
		verifierResultatTest(88, liste, true); 
    }
    
    static void verifierResultatTest(int valeurCherchee, ArrayList<Integer> liste, boolean resultatAttendu)
    {
		boolean resultatRecherche = Egl.rechercheFausseBool(valeurCherchee, liste);
		if ( resultatRecherche != resultatAttendu )
		{
			System.out.print ("Résultats contradictoires  :");
			if ( resultatRecherche )
				System.out.println ("Valeur "+valeurCherchee+" trouvée dans "+liste);
			else
				System.out.println ("Valeur "+valeurCherchee+" absente de "+liste);
		}
    }
    
    static ArrayList<Integer> remplirAvec(Integer[] tableau)
    {
 		ArrayList<Integer> a = new ArrayList<Integer>();
    	java.util.Collections.addAll(a, tableau); 
    	return a;
    }
	
}


